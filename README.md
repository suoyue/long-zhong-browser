# LongZhongBrowser

#### 介绍
隆中对浏览器

项目文档汇总地址：[https://bytedancecampus1.feishu.cn/docs/doccnBKhVTkX1kR4qSMg5CTpgbc?from=from_copylink](https://bytedancecampus1.feishu.cn/docs/doccnBKhVTkX1kR4qSMg5CTpgbc?from=from_copylink)



5.6(adblock广告拦截)



参考项目代码：[penk/minimal-webbrowser-adblock: A Minimal Web Browser with Built-in Adblocker in Less Than 100 Lines of Code (github.com)](https://github.com/penk/minimal-webbrowser-adblock)

可能编译出问题，按照项目中给出的配置进行排查，注意在.pro当中添加qmake配置，下图为成功结果



出现cpp use adblock成功，可以把easylist.txt放入文件所在文件夹中，不然会有提示。进入网站时有出现Blocked的输出即为成功（成功率感人，这是加了自定义拦截配置才有的）

[![OnRrqg.png](https://s1.ax1x.com/2022/05/06/OnRrqg.png)](https://imgtu.com/i/OnRrqg)

[![OnR4MT.md.png](https://s1.ax1x.com/2022/05/06/OnR4MT.md.png)](https://imgtu.com/i/OnR4MT)
