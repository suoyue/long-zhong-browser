﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtWebEngine/QtWebEngine>
#include <QtWebEngine/qtwebengineglobal.h>
#include <QtWebEngine/qquickwebengineprofile.h>
#include <QtWebEngineCore/qwebengineurlrequestinterceptor.h>
#include <QQuickImageProvider>
#include <QQmlContext>
#include <QDebug>
#include <QThread>
#include <QFile>
#include "managers/user.h"
#include "third_party/requestinterceptor.h"
#include "managers/downloadmanager.h"
#include "managers/historymanager.h"
#include "managers/bookmarkmanager.h"
#include "managers/downloadmanager.h"

int main(int argc, char *argv[])
{
    QtWebEngine::initialize();
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QNetworkProxyFactory::setUseSystemConfiguration(false);
    QCoreApplication::setOrganizationName("LongZhong");
    QGuiApplication app(argc, argv);

    qmlRegisterType<HistoryManager>("HistoryManager",1,0,"HistoryManager");
    qmlRegisterType<BookmarkManager>("BookmarkManager",1,0,"BookmarkManager");
    qmlRegisterType<DownLoadManager>("DownloadManager",1,0,"DownloadManager");
    qmlRegisterType<User>("User",1,0,"User");
    QQmlApplicationEngine engine;

    const QUrl url(QStringLiteral("qrc:/qml/view/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    //adblock 相关
#ifdef USE_ADBLOCK
    qDebug()<<"use adblock";
    RequestInterceptor interceptor;
    QQuickWebEngineProfile adProfile;
    QObject *pRoot = engine.rootObjects().first();
    QQuickWebEngineProfile *pProfile = pRoot->findChild<QQuickWebEngineProfile *>("adblockProfile");
    if( pProfile ) {
        qDebug()<<"get profile success";
        pProfile->setUrlRequestInterceptor(&interceptor);
        engine.rootContext()->setContextProperty("adblockProfile", pProfile);
    }else{
        qDebug()<<"get profile failed";
    }
#endif

    return app.exec();
}
#include "main.moc"
