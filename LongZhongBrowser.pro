QT += qml quick webengine

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += USE_ADBLOCK

contains(DEFINES, USE_ADBLOCK) {
        SOURCES += third_party/ad-block/ad_block_client.cc \
                third_party/ad-block/no_fingerprint_domain.cc \
                third_party/ad-block/filter.cc \
                third_party/ad-block/protocol.cc \
                third_party/ad-block/context_domain.cc \
                third_party/ad-block/cosmetic_filter.cc \
                third_party/bloom-filter-cpp/BloomFilter.cpp \
                third_party/hashset-cpp/hash_set.cc \
                third_party/hashset-cpp/hashFn.cc


        HEADERS = third_party/ad-block/ad_block_client.h

}

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        managers/bookmarkitem.cpp \
        managers/bookmarkmanager.cpp \
        managers/downloaditem.cpp \
        managers/downloadmanager.cpp \
        managers/historyitem.cpp \
        managers/historymanager.cpp \
        managers/user.cpp \
        third_party/requestinterceptor.cpp

RESOURCES += qml.qrc

RC_FILE = logo.rc
# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH ="D:\qt6\5.15.2\msvc2019_64\qml"

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    managers/bookmarkitem.h \
    managers/bookmarkmanager.h \
    managers/downloaditem.h \
    managers/downloadmanager.h \
    managers/historyitem.h \
    managers/historymanager.h \
    managers/user.h \
    third_party/requestinterceptor.h \
