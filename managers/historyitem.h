﻿#ifndef HISTORYITEM_H
#define HISTORYITEM_H

#include <QObject>

#include <QMetaType>
#include<QDateTime>
class HistoryItem: public QObject
{
    Q_GADGET
    Q_PROPERTY(QString url READ url WRITE setUrl)
    Q_PROPERTY(QString title READ title WRITE setTitle)
    Q_PROPERTY(QString icon READ icon WRITE setIcon)
    Q_PROPERTY(QDateTime time READ time WRITE setTime)
    Q_PROPERTY(int id READ id WRITE setId)

public:
    HistoryItem();
    ~HistoryItem();
    HistoryItem(const HistoryItem& other);
    HistoryItem(QString url,QString title,QString icon,QDateTime time,int id);

    QString url() const;
    QString title() const;
    QString icon() const;
    QDateTime time()const;
    int id() const;

    void setUrl(QString);
    void setTitle(QString);
    void setIcon(QString);
    void setTime(QDateTime);
    void setId(int id);

private:
    int m_id;
    QString m_url;
    QString m_title;
    QString m_icon;
    QDateTime m_time;

};
Q_DECLARE_METATYPE(HistoryItem)

#endif // HISTORYITEM_H
