﻿#ifndef USER_H
#define USER_H

#include<QObject>
#include<QStringList>
#include<QJsonArray>
#include<QTextStream>
#include"historymanager.h"
#include"bookmarkmanager.h"
#include"downloadmanager.h"
#define DOWNF "downloadfolder"
#define USN "username"
#define PSW "password"
#define downloadPath "C:/qtbrowser/download"
#define PRO "profile"
class User:public QObject
{
    Q_OBJECT
    Q_PROPERTY(HistoryManager* historyManager READ historyManager NOTIFY userChanged)
    Q_PROPERTY(BookmarkManager* bookmarkManager READ bookmarkManager NOTIFY userChanged)
    Q_PROPERTY(DownLoadManager* downloadManager READ downloadManager NOTIFY userChanged)
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(QString password READ password)
    Q_PROPERTY(QString profile READ profile NOTIFY profileChanged);
    Q_PROPERTY(QString download READ  download NOTIFY downloadChanged);
    Q_PROPERTY(bool logStatus READ logStatus  NOTIFY logStatusChanged)
public:
    static QMap<QString,QString> map;
    static User* cUser;
    User();
    User(QString name,QString password);
    ~User();

    QString name() const;
    QString password() const;
    QString profile() const;
    QString download() const;
    HistoryManager* historyManager() const;
    BookmarkManager* bookmarkManager()  const;
    DownLoadManager* downloadManager()const;
     bool logStatus()const;
    //作为一个加载用户的函数
    //void loadUser();
    QString getuserfolder(QString& usn);
    QString getuserfile(QString& path);
    //Q_INVOKABLE User* currentUser();


    Q_INVOKABLE  bool login(QString,QString);
    //logout和login相似,读取默认用户的文件
    Q_INVOKABLE void logout();
    Q_INVOKABLE void createUser( QString usn,const QString psw);
    Q_INVOKABLE void changeUserName(QString name);
    Q_INVOKABLE void changeDownloadFolder(QString foldername);
    Q_INVOKABLE void changeprofile(QString filename);
    Q_INVOKABLE void changepassword(QString psw);
    QJsonObject makejsonobj();
    QJsonObject readjsonobj(const QString& filename);
    //从json中读取用户信息
    void setCharater(QString& username);
    //用户文件的保存其中调用了makejsonobj()
    void saveMesage();
    void remove_history_bookmark();
    //辅助函数在setcharater里调用
    void set_history_bookmark();
signals:
    void sendMsg(const QString& str);
    void userChanged();
    void downloadChanged();
    void profileChanged();
    void nameChanged();
    void logStatusChanged();
private:
    QString m_name,m_password;
    HistoryManager* m_historyManager;
    BookmarkManager* m_bookmarkManager;
    DownLoadManager* m_downloadManager;



    QString m_profile;
    bool iflogin;
    QString m_downloadFolder;
};



#endif // USER_H
