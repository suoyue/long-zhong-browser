﻿#include "downloaditem.h"
#include<QDebug>
DownLoadItem::DownLoadItem()
{

}
DownLoadItem::DownLoadItem(const DownLoadItem& other){
    m_id=other.m_id;
    m_name=other.m_name;
    m_path=other.m_path;
    m_time=other.m_time;
}
DownLoadItem::DownLoadItem(QString path,QString time,QString name,int id){
    m_time=time;
    m_path=path;
    m_name=name;
    m_id=id;
}
QString DownLoadItem:: path() const{
    return m_path;
}
QString DownLoadItem:: time() const{
    return m_time;
}

QString DownLoadItem::  name() const{
    return m_name;
}


int DownLoadItem:: id() const{
    return m_id;
}
void DownLoadItem:: setPath(QString path){
    m_time=path;
}
void DownLoadItem:: setTime(QString time){
    m_time=time;
}

void DownLoadItem:: setName(QString name){
    m_name=name;
}

void DownLoadItem:: setId(int id){
    m_id=id;
}
