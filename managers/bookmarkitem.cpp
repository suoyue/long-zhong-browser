#include"bookmarkitem.h"

BookmarkItem::BookmarkItem()
{

}

BookmarkItem::~BookmarkItem()
{

}

QString BookmarkItem::url() const
{
    return m_url;
}

QString BookmarkItem::title() const
{
    return m_title;
}

QString BookmarkItem::icon() const
{
    return m_icon;
}

BookmarkItem::BookmarkItem(const BookmarkItem &other)
{
    m_url=other.m_url;
    m_title=other.m_title;
    m_icon=other.m_icon;
}

BookmarkItem::BookmarkItem(QString url, QString title, QString icon)
{
    m_url=url;
    m_title=title;
    m_icon=icon;
}

void BookmarkItem::setUrl(QString url)
{
    m_url=url;
}

void BookmarkItem::setTitle(QString title)
{
    m_title=title;

}

void BookmarkItem::setIcon(QString icon)
{
    m_icon=icon;
}
