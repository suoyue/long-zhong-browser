﻿#ifndef DOWNLOADITEM_H
#define DOWNLOADITEM_H
#include <QObject>

class DownLoadItem:public QObject
{
    Q_GADGET
    Q_PROPERTY(QString time READ time WRITE setTime)
    Q_PROPERTY(QString path READ path WRITE setPath)
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(int id READ id WRITE setId)

public:
    DownLoadItem();
    DownLoadItem(const DownLoadItem& other);
    DownLoadItem(QString path,QString time,QString name,int id);

    QString time() const;
    QString path() const;
    QString name() const;
    int id() const;

    void setPath(QString);
    void setTime(QString);
    void setName(QString);
    void setId(int id);

private:
    QString m_path;
    int m_id;
    QString m_time;
    QString m_name;

};
Q_DECLARE_METATYPE(DownLoadItem)
#endif // DOWNLOADITEM_H
