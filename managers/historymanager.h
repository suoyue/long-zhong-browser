﻿#ifndef HISTORYMANAGER_H
#define HISTORYMANAGER_H

#include <QObject>
#include<QDebug>
#include"historyitem.h"
#include <QQmlListProperty>
#include<QDateTime>
#include<QJsonObject>
#include<QFile>

class HistoryManager:public QObject
{
    Q_OBJECT
public:
    HistoryManager();
    HistoryManager(QString);
    ~HistoryManager();
    //add
    Q_INVOKABLE void add(QString url,QString icon,QString title,QDateTime time);

    Q_INVOKABLE void remove(int);
    Q_INVOKABLE void removeAll();
    Q_INVOKABLE QVariantList getHistoryItemList();

    static int id;

signals:

public slots:

private:

    QVariantList list;
    QFile file;
};

#endif // HISTORYMANAGER_H
