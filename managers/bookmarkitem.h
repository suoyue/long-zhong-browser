#ifndef BOOKMARKITEM_H
#define BOOKMARKITEM_H


#include <QObject>

#include <QMetaType>
class BookmarkItem: public QObject
{
    Q_GADGET
    Q_PROPERTY(QString url READ url WRITE setUrl)
    Q_PROPERTY(QString title READ title WRITE setTitle)
    Q_PROPERTY(QString icon READ icon WRITE setIcon)


public:
    BookmarkItem();
    ~BookmarkItem();
    BookmarkItem(const BookmarkItem& other);
    BookmarkItem(QString url,QString title,QString icon);

    QString url() const;
    QString title() const;
    QString icon() const;


    void setUrl(QString);
    void setTitle(QString);
    void setIcon(QString);

private:
    QString m_url;
    QString m_title;
    QString m_icon;

};
Q_DECLARE_METATYPE(BookmarkItem)

#endif // BOOKMARKITEM_H
