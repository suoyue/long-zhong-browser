﻿#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H



#include <QObject>
#include <QDebug>
#include "downloaditem.h"
#include <QQmlListProperty>
#include <QDateTime>
#include <QJsonObject>
#include <QFile>
#include <QDesktopServices>


class DownLoadManager:public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList list READ getDownLoadItemList NOTIFY listChanged);
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged);

public:
    DownLoadManager();
    DownLoadManager(QString name);
    ~DownLoadManager();

    QString getPath();
    void setPath(QString t_path);
    Q_INVOKABLE void add(QString path,QString name,QString time);
    Q_INVOKABLE void remove(int id);
    Q_INVOKABLE void removeAll();
    Q_INVOKABLE bool open(QString path);


    QVariantList getDownLoadItemList();
signals:
    void listChanged();
    void pathChanged();

public slots:

private:
    QVariantList list;
    QFile file;
    QString path;
    static int id;
};

#endif // DOWNLOADMANAGER_H
