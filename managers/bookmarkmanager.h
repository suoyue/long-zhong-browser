﻿#ifndef BOOKMARKMANAGER_H
#define BOOKMARKMANAGER_H

#include <QObject>
#include<QDebug>
#include"bookmarkitem.h"
#include <QQmlListProperty>
#include<QDateTime>
#include<QJsonObject>
#include<QFile>

class BookmarkManager:public QObject
{
    Q_OBJECT
public:
    BookmarkManager();
    BookmarkManager(QString);
    ~BookmarkManager();

    Q_INVOKABLE void add(QString url,QString icon,QString title);
    Q_INVOKABLE QVariantList getBookmarkItemList();

    Q_INVOKABLE void remove(QString url);
    Q_INVOKABLE bool isFavorited(QString url);

signals:

public slots:

private:

    QVariantList list;
    QFile file;
};

#endif // BOOKMARKMANAGER_H
