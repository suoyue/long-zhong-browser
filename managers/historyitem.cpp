﻿#include "historyitem.h"

HistoryItem::HistoryItem()
{

}

HistoryItem::~HistoryItem()
{

}

HistoryItem::HistoryItem(const HistoryItem &other)
{
    m_url=other.m_url;
    m_title=other.m_title;
    m_icon=other.m_icon;
    m_time=other.m_time;
    m_id=other.m_id;

}

HistoryItem::HistoryItem(QString url, QString title, QString icon, QDateTime time,int id)
{
    m_url=url;
    m_title=title;
    m_icon=icon;
    m_time=time;
    m_id=id;
}

QString HistoryItem::url() const
{
    return m_url;
}

QString HistoryItem::title() const
{
    return m_title;
}

QString HistoryItem::icon() const
{
    return m_icon;
}

QDateTime HistoryItem::time() const
{
    return m_time;
}

int HistoryItem::id() const
{
    return m_id;
}


void HistoryItem::setUrl(QString url)
{
    m_url=url;
}

void HistoryItem::setTitle(QString title)
{
    m_title=title;

}

void HistoryItem::setIcon(QString icon)
{
    m_icon=icon;
}

void HistoryItem::setTime(QDateTime time)
{
    m_time=time;
}

void HistoryItem::setId(int id)
{
    m_id=id;
}

