﻿import QtQuick 2.0

Rectangle {
    id:root
    property var icon
    width: 36
    height: 36
    color: "transparent"

    AnimatedImage {
        id:busy
        anchors.fill: parent
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        z:2
        source: "../../src/loading.gif"
        playing:true
        visible: true
    }

    Image {
        id:imageViews
        anchors.fill: parent
        anchors.centerIn: parent
        cache: false
        asynchronous: true
        fillMode: Image.PreserveAspectFit
        source: icon

        onStatusChanged: {
            if(imageViews.status == Image.Loading){
                busy.visible = true;
                busy.playing=true
            }
            else if(imageViews.status == Image.Ready)
            {
                busy.visible = false;
                busy.playing=false;
            }
            else if(imageViews.status == Image.Error)
            {
                busy.visible = false;
                busy.playing=false;
                root.source="../../src/icon_default.png"
            }
        }
    }

}



