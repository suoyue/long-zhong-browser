﻿import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.14


Rectangle{
    z:3;
    height: 100;
    property bool isMaximized: true;
    color:"#DDDFE2"

    MouseArea{
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton //只处理鼠标左键
        property bool   isDoubleClicked:false
        property point clickPos: "0,0"
         onPressed:
         {
             isDoubleClicked = false;
             clickPos = Qt.point(mouse.x,mouse.y)
         }
         onPositionChanged: {
             if(!isDoubleClicked && pressed && mainWindow.visibility !== Window.Maximized && mainWindow.visibility !== Window.FullScreen) {
                 var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y)
                 mainWindow.x += delta.x
                 mainWindow.y += delta.y
             }
             if(mainWindow.visibility === Window.Maximized && pressed && !isDoubleClicked)
             {
                 isMaximized = false;
                 mainWindow.showNormal();
             }
         }
         onDoubleClicked :
         {
             isDoubleClicked = true; // 这个时候一定不能响应onPositionChanged不然会一直置顶。
             if(isMaximized){
                 isMaximized = false;
                 mainWindow.showNormal();
             }else{
                 isMaximized = true;
                 mainWindow.showMaximized();
             }
         }
     }


    Image{
        id:icon
        anchors{
            top: parent.top;
            topMargin: 5
            bottom: parent.bottom
            bottomMargin: 5
            left: parent.left
            leftMargin: 20
        }
        width: icon.height
        fillMode: Image.PreserveAspectFit
        source: "../../src/browser.png"
    }
    Text {
        id: text
        text: "隆中浏览器"
        anchors{
            top: parent.top;
            bottom: parent.bottom
            left: icon.right
            leftMargin: 10
        }
        verticalAlignment: Text.AlignVCenter//(2)
    }

    MyButton{
        id:closeBtn
        anchors{right: parent.right; rightMargin: 10; top: parent.top;
                topMargin: 0;bottom: parent.bottom;}
        width: 50; height:50;

        icon_default: "../../src/title_close.png"
        m_padding: 8
        m_radius: 0
        onClicked: {
            mainWindow.close()
        }
    }
    MyButton{
        id: normBtn;
        icon_default: isMaximized?"../../src/title_min":"../../src/title_full.png"

        anchors{right: closeBtn.left; rightMargin: 10; top: parent.top;
                topMargin: 0;bottom: parent.bottom;}
        width: 50; height:50;
        m_padding: 8
        m_radius: 0

        onClicked:{
            if(isMaximized){
                isMaximized = false;
                mainWindow.showNormal();
            }else{
                isMaximized = true;
                mainWindow.showMaximized();
            }
        }
    }
    MyButton{
        id: minBtn
        anchors{right: normBtn.left; rightMargin: 10; top: parent.top;
                topMargin: 0;bottom: parent.bottom;}
        width: 50; height:50;
        icon_default: "../../src/title_hide.png"
        m_padding: 8
        m_radius: 0
        onClicked:{
            mainWindow.showMinimized();
        }
    }
}
