﻿import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import QtWebEngine 1.10
import QtQuick.Layouts 1.0
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.0
import "../js/MyTool.js" as MyTool
Item{
    id:container

    property alias imageSource:logo.source
    property alias imageWidth:logo.width
    property alias imageHeight:logo.height

    signal clicked

    property alias containsMouse:mouseArea.containsMouse
    property alias pressed:mouseArea.pressed

    Rectangle{
        id:bg
        anchors.fill:parent
        color:"white"
        opacity:0
        radius:2
    }

    MouseArea{
        id:mouseArea
        anchors.fill:parent
        onClicked:{
            container.clicked()
        }

        onReleased:{
            logo.anchors.horizontalCenterOffset=0
            logo.anchors.verticalCenterOffset=0
        }

        onPressed:{
            logo.anchors.horizontalCenterOffset=0.4
            logo.anchors.verticalCenterOffset=0.4
        }

        hoverEnabled:true
        onEntered:{
            bg.opacity=0.3
        }

        onExited:{
            bg.opacity=0
        }
    }

    Image{
        id:logo
        source:"close.png"
        anchors.centerIn:parent

        width:20
        height:20
    }
}


