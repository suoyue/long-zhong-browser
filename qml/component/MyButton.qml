﻿import QtQuick 2.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.4
ToolButton {
    id:root

    property var icon_default
    property var icon_disabled
    property int m_padding: 0
    property int m_radius: 8

    width: 40
    height:40

    style: ButtonStyle {
        padding{
            left:root.m_padding
            right:root.m_padding
            top:root.m_padding
            bottom:root.m_padding
        }

        background: Rectangle {
            anchors.fill: parent
            radius: root.m_radius
            color: {
                if(control.hovered)
                {
                    if(control.pressed)
                    {
                        Qt.rgba(0,0,0,0.5)
                    }
                    else
                    {
                        Qt.rgba(0,0,0,0.25)
                    }
                }
                else
                {
                   Qt.rgba(0,0,0,0)
                }
            }

            gradient: Gradient {
                GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
            }
        }
        label: Image {
            source: root.enabled?icon_default:(icon_disabled?icon_disabled:icon_default)
            fillMode: Image.PreserveAspectFit
        }
    }
}
