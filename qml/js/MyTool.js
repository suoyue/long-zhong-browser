﻿//获取文件MINE_TYPE对应的icon的source
//fileName:名字 type:MIME TYPE
function getFileIcon(fileName,type) {
    let arr=fileName.split('.')
    let name=arr[arr.length-1]
    arr=type.split('/')
    type=arr[0]

    //exe
    if(name==="exe"){
        return "../../src/type_exe.png"
    }
    //image
    if(type==="image"||name==="png"||name==="jpg"||name==="jpeg"||name==="gif"){
        return "../../src/type_image.png"
    }
    //audio
    if(type==="audio"||name==="au"||name==="mp3"){
        return "../../src/type_audio.png"
    }
    //video
    if(type==="video"||name==="mp4"||name==="avi"||name==="flv"){
        return "../../src/type_video.png"
    }
    //text
    if(type==="text"||name==="txt"||name==="pdf"){
        return "../../src/type_txt.png"
    }

    return "../../src/type_unknown.png"
}

//获取网页对应ico地址
function getWebIcoUrl(url){
    var reg=/(\w+:\/\/)([^/:]+)/;
    if(reg.test(url)&&url!==""){
        var iconUrl=reg.exec(url)[0]+"\/favicon.ico"
        return iconUrl
    }else{
        return ""
    }
}

function dateToString(date){
    return date.toLocaleString(Qt.locale("de_DE"), "yyyy-MM-dd HH:mm:ss")
}

function dateToString2(date){
    date=date.split(" ")
    date.splice(4,2)
    return date.join(' ')
}

function dateTimePassTag(time){
    //给每条历史记录加标签，标记访问的时间距当前时间有多久
    var now = new Date()
    var timePass = (now - time) // 单位为毫秒
    var timePassTag = ""
    if(timePass <= 1 * 60 * 60 * 1000){
        timePassTag = "一小时内"
    }
    else if(1 * 60 * 60 * 1000 < timePass && timePass <= 24 * 60 * 60 * 1000){
        timePassTag = "一天内"
    }
    else if(24 * 60 * 60 * 1000 < timePass && timePass <= 7 * 24 * 60 * 60 * 1000){
        timePassTag = "一周内"
    }
    else{
        timePassTag = "超过一周"
    }
    return timePassTag
}



