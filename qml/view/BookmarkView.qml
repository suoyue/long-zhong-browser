﻿import QtQuick 2.6
import QtQuick.Window 2.2
import QtWebEngine 1.10
import QtQuick.Controls.Styles 1.0
import QtQuick.Controls 1.4
import HistoryManager 1.0
import BookmarkManager 1.0
import DownloadManager 1.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14
import User 1.0
import "../js/MyTool.js" as MyTool
import "../component/"

Rectangle{

    ListModel{
        id: bookmarkList
    }

    function getList(){
        var list=bookmarkManager.getBookmarkItemList()
        bookmarkList.clear()
        console.log("qml bookmark length:",list.length)
        list.reverse()
        var number = 0
        for(let item of list){
            // 向ListModel中添加记录
            bookmarkList.append({title: item.title, icon: item.icon, url: item.url, number: number})
            number++
        }
    }

    //向ListModel中存放历史记录
    Component.onCompleted: getList()


    // 展示样式设计
    Component{
        id: show
        Rectangle{
            id: historyRectangle

            width: historyView.width
            height: 30
            border.width: 1
            border.color: "grey"  // 每条历史记录用灰色框
            radius: 3
            // 展示icon
            Image{
                id: historyIcon
                width: 20
                height: 20

                source: icon
                anchors{
                    left: parent.left
                    leftMargin: 20
                    top:parent.top
                    topMargin: 5
                }
                onStatusChanged: {
                    if(historyIcon.status == Image.Error)
                    {
                        source="../../src/icon_default.png"
                    }
                }
            }

            // 展示历史记录标题
            Text{
                id: historyText1
                elide: Text.ElideRight
                text: title
                horizontalAlignment: Text.AlignLeft
                anchors{
                    verticalCenter: parent.verticalCenter
                    right: historyText2.left
                    rightMargin: 10
                    left: historyIcon.right
                    leftMargin: 10
                }

                MouseArea{
                    id: historyRectangleMouse1
                    anchors.fill: parent
                    hoverEnabled: true

                    // 鼠标移动到历史记录上时改变样式
                    onEntered: {
                        historyRectangleMouse1.cursorShape = Qt.PointingHandCursor
                        historyRectangle.color = "grey"
                        historyText1.color = "yellow"
                        historyText2.color = "yellow"
                    }

                    // 鼠标离开历史记录上时恢复原来的样式
                    onExited: {
                        historyRectangleMouse1.cursorShape = Qt.ArrowCursor
                        historyRectangle.color = "white"
                        historyText1.color = "balck"
                        historyText2.color = "balck"
                    }

                    // 点击历史记录打开对应页面
                    onClicked: function(){
                        var tab = tabs.addTab("", tabComponent);
                        tab.active = true;
                        tab.title = title;
                        tab.item.url = url;
                        tabs.addIconList();
                        tabs.currentIndex = tabs.count - 1;
                    }

                }

            }



            // 展示历史记录的访问时间
            Text{
                id: historyText2
                width: 300
                elide: Text.ElideRight
                anchors{
                    verticalCenter: parent.verticalCenter
                    right: deleteHistory.left
                    rightMargin: 10
                }

                text: url


                MouseArea{
                    id: historyRectangleMouse2
                    anchors.fill: parent
                    hoverEnabled: true

                    // 鼠标移动到历史记录上时改变样式
                    onEntered: {
                        historyRectangleMouse2.cursorShape = Qt.PointingHandCursor
                        historyRectangle.color = "grey"
                        historyText1.color = "yellow"
                        historyText2.color = "yellow"
                    }

                    // 鼠标离开历史记录上时恢复原来的样式
                    onExited: {
                        historyRectangleMouse2.cursorShape = Qt.ArrowCursor
                        historyRectangle.color = "white"
                        historyText1.color = "balck"
                        historyText2.color = "balck"
                    }

                    // 点击历史记录打开对应页面
                    onClicked: function(){
                        var tab = tabs.addTab("", tabComponent);
                        tab.active = true;
                        tab.title = title;
                        tab.item.url = url;
                        tabs.addIconList();
                        tabs.currentIndex = tabs.count - 1;
                    }

                }

            }
            // 删除单条历史记录
            Image{
                id: deleteHistory
                width: 20
                height: 20
                source: "../../src/delete.png"

                anchors{
                    right: parent.right
                    rightMargin: 20
                    top:parent.top
                    topMargin: 5
                }

                MouseArea{
                    id: deleteMouse
                    anchors.fill: parent

                    onClicked: function(){
                        // ListModel和historyManagement中都需要删除
                        bookmarkManager.remove(url)
                        bookmarkList.remove(index)
                    }
                }
            }

        }


    }


    Rectangle{
        id: row_top
        height: 70

        anchors.top: parent.top
        anchors.topMargin: 0
        color:"white"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        z:1
        Text{
            id:text_title
            text:"收藏夹"
            font.pointSize: 18
            font.family: "Arial"
            anchors.left: parent.left
            anchors.leftMargin: 25
            anchors.verticalCenter: parent.verticalCenter
        }
        // 清除全部历史记录
        Button{
            id: historyClear
            width: 100
            height: 40
            anchors.top: parent.top
            anchors.topMargin: 10
            text: "清除全部收藏记录"
            anchors.right: parent.right
            anchors.rightMargin: 40

            onClicked: function(){
                // ListModel和historyManagement中都需要删除
                bookmarkList.clear()
                bookmarkManager.removeAll()
            }

        }

    }


    ListView{
        id: historyView
        anchors.top: row_top.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        model: bookmarkList
        anchors.topMargin: 0
        spacing: 3
        anchors.leftMargin: 20
        anchors.rightMargin: 20
        delegate: show

    }

}
