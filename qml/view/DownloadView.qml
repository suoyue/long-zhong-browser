﻿import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import QtWebEngine 1.10
import QtQuick.Layouts 1.0
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.0
import "../js/MyTool.js" as MyTool
import "../component/"
Rectangle {
    id: downloadView
    color: "white"

    width:450
    height:700
    radius: 8

    property var btnClicked

    ListModel {
        id: downloadModel
        property var downloads: []
    }

    layer.enabled: true
    layer.effect: DropShadow {
        anchors.fill: downloadView
        horizontalOffset: 3
        verticalOffset: 3
        radius: 8.0
        samples: 16
        color: "#80000000"
        source: downloadView
    }
    function append(download) {
        downloadModel.append(download);
        downloadModel.downloads.push(download);
    }

    Component {
        id: downloadItemDelegate
        Rectangle {
            id:rect_item
            width: listView.width
            height: 80
            color: "white"
            border.color: "black"
            border.width: 1

            Image {
                id: image
                y: 0
                width: 30
                height: 30
                anchors.left: parent.left
                anchors.leftMargin: 8
                anchors.verticalCenter: parent.verticalCenter
                fillMode: Image.PreserveAspectFit
                source: MyTool.getFileIcon(downloadFileName,mimeType)
            }
            Column{
                id:column
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                anchors.top: parent.top
                anchors.topMargin: 12
                anchors.right: column2.left
                anchors.rightMargin: 8
                anchors.left: image.right
                anchors.leftMargin: 8
                spacing: 20

                Text {
                    id: text1
                    width:parent.width
                    text: downloadFileName
                    font.family: "Arial"
                    elide: Text.ElideRight
                    font.pixelSize: 18
                }

                Text {
                    id: text2
                    width:parent.width
                    color: "#5d5d5d"
                    elide: Text.ElideRight
                    text: downloadDirectory + "/" + downloadFileName
                    font.pixelSize: 12
                }

            }
            Rectangle{
                id: column2
                width: 150
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.right: btn_pause.left
                anchors.rightMargin: 5

                property real progress: downloadModel.downloads[index]
                                        ? downloadModel.downloads[index].receivedBytes / downloadModel.downloads[index].totalBytes : 1

                ProgressBar {
                    id: progressBar
                    height: 8
                    anchors.left: parent.left
                    anchors.leftMargin: 1
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    anchors.right: parent.right
                    anchors.rightMargin: 1
                    value: column2.progress?column2.progress:1
                }
                Text {
                    id: text3
                    text: "下载中 --- 50%"
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 6
                    anchors.right: parent.right
                    anchors.rightMargin: 2
                    font.pixelSize: 16
                }
                onProgressChanged: {
                    text3.text="下载中 -- "+(progress*100).toFixed(2)+"%"
                    if(progress==1){
                        //todo
                        progressBar.value=1
                        text3.text="下载完成"
                        column2.visible=false
                        column3.visible=true
                        btn_pause.visible=false
                    }
                }
            }
            Rectangle{
                id: column3
                width: 150
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.right: image1.left
                anchors.rightMargin: 8
                visible: false
                Text {
                    id: text4
                    text: "下载完成"
                    anchors.left: parent.left
                    anchors.leftMargin: 4
                    font.pixelSize: 18
                    anchors.verticalCenter: parent.verticalCenter
                }

            }

            MyButton{
                id:btn_pause
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: image1.left
                anchors.rightMargin: 5
                width: 30
                height: 30
                m_padding: 4
                icon_default:"../../src/pause.png"
                property var flag: true
                onClicked: {
                    var download = downloadModel.downloads[index];
                    if(flag){
                        download.pause();
                        text3.text="下载暂停"
                        icon_default="../../src/start.png"

                    }else{
                        download.resume();
                        text3.text="下载继续"
                        icon_default="../../src/pause.png"
                    }
                    flag=!flag
                }
            }


            MyButton {
                id: image1
                width: 30
                height: 30
                m_padding: 4
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 5
                icon_default:"../../src/delete.png"

                onClicked:{
                    var download = downloadModel.downloads[index];

                    download.cancel();

                    downloadModel.downloads = downloadModel.downloads.filter(function (el) {
                        return el.id !== download.id;
                    });
                    downloadModel.remove(index);
                }
            }
        }

    }

    MouseArea{
        anchors.fill: parent
        Rectangle{
            id: row_top
            height: 50
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            border.color: "black"
            border.width: 1
            color:"white"
            z:1
            Text{
                text:"下载中"
                font.pointSize: 15
                font.family: "Arial"
                anchors.left: parent.left
                anchors.leftMargin: 30
                anchors.verticalCenter: parent.verticalCenter
            }
            Button{
                id:btn_more
                text:"下载历史"
                anchors.right: parent.right
                anchors.rightMargin: 30
                anchors.verticalCenter: parent.verticalCenter
                onClicked: btnClicked()
            }
        }
        Rectangle{
            id: border2
            height: 1
            anchors.top: row_top.bottom
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            z:1
            color:"black"

        }
        ListView {
            id: listView
            anchors {
                topMargin: 10
                top: row_top.bottom
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter
            }
            width: parent.width

            model: downloadModel
            delegate: downloadItemDelegate

            Text {
                visible: !listView.count
                horizontalAlignment: Text.AlignHCenter
                height: 30
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
                font.pixelSize: 20
                text: "暂无正在下载的文件"
            }
        }
    }




}


