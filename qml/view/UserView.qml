﻿import QtQuick 2.0
import User 1.0
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import "../component/"
Window{
//Rectangle{
    id:usrView
    property User user2: user1
    //修改比例
    property double rage : 7.5/8
    title:qsTr("用户管理")
    minimumHeight: 400
    maximumHeight: 400
    minimumWidth: 350
    maximumWidth: 350
    //rec0是基本界面
    Rectangle{
        id:rec0
        width: parent.width
        height: parent.height * rage
    Image {
        id: userProfile
        width:100
        height:100
        source:  user2.profile
        visible: true;
    }
    Rectangle{
        id:rec1
        width: 200
        height:100
        anchors.left: userProfile.right
        anchors.leftMargin: 30
        anchors.topMargin: 50
        radius: 5
        color: "white"
        Text{
            anchors.topMargin: 20
            id: usn
            text: user2.name
            font.bold: true
            font.pixelSize: 25
            elide: Text.ElideRight
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Button{
            id:btn1
            anchors.top:usn.bottom
            anchors.topMargin: 10
            text: qsTr("更改用户名")
            font.pixelSize: 14
            width: parent.width / 2
            height: parent.height/3
            visible:false
        }
        Button{
            id:btn2
            text: "更改密码"
            anchors.top: usn.bottom
            anchors.topMargin: 10
            anchors.left: btn1.right
            anchors.leftMargin: -20
            width: parent.width/2.5
            height: parent.height/3
            font.pixelSize: 14
            visible: user1.logStatus
            onClicked: passwordRec.visible = true;
        }

    }
    Rectangle{
        id:rec2
        anchors.top:userProfile.bottom
        width: userview.width
        height:130
        anchors.topMargin: 10
        color: "white"
        Text {
            id: text1
            text: qsTr("下载路径")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 10
        }
        Text {
            id: folder
            text: user1.downloadManager.path
            anchors.top: text1.bottom
            width:parent.width
            horizontalAlignment: Text.AlignHCenter
            anchors.topMargin: 10
            font.pixelSize:12
            wrapMode: Text.WrapAnywhere
            maximumLineCount:2
            elide: Text.ElideRight
        }
        Button{
            id:btnFolder
            anchors.top:folder.bottom
            anchors.topMargin: 30
            text: qsTr("更换下载目录")
            onClicked: fds.open()
            visible: user1.logStatus
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Button{
            id:btnRegist
            anchors.top:folder.bottom
            anchors.topMargin: 30
            text: qsTr("注册")
            onClicked: registRec.visible = true;
            visible: !user1.logStatus
            anchors.horizontalCenter: parent.horizontalCenter
        }

    }
    Rectangle{
        id:rec3
        width: userview.width
        anchors.top: rec2.bottom
        anchors.topMargin: 20
        height:parent.height / 8
        Button{
            id: changeProfile
            anchors.horizontalCenter: parent.horizontalCenter
            text:qsTr("更换头像文件")
            onClicked: fd.open()
            visible: user1.logStatus
        }
        Button{
            id: btnLog
            anchors.horizontalCenter: parent.horizontalCenter
            text:qsTr("登录")
            onClicked: loginRec.visible = true
            visible: !user1.logStatus
        }
    }
    Rectangle{
        id:rec4
        anchors.top: rec3.bottom
        anchors.topMargin: 20
        height: parent.height / 8
        width: parent.width
        Button{
            id: btnquit
            text: qsTr("退出登录")
            visible: user1.logStatus

//            anchors.right: parent.right
//            anchors.rightMargin: parent.width / 6
            anchors.centerIn:parent
            onClicked: user1.logout()

        }
    }
}
    property string msg;
    onMsgChanged: {
        message.text = msg;
    }

    Rectangle{
        id:rec5
        anchors.top: rec0.bottom
        width: parent.width
        visible: true
        Text {
            id: message
//            horizontalAlignment: parent.horizontalCenter
//            anchors.centerIn: parent.center
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: true
        }
    }
    Component.onCompleted: {
        user1.sendMsg.connect(dialog);
    }
    function dialog(str){
        msg = str
    }

    FileDialog{
        id: fd
        title:"选择一个头像文件"
        selectFolder: false
        selectMultiple: false
        selectExisting: true
        nameFilters: ["jpg文件(*.jpg)","png文件(*.png)","jpeg文件(*.jpeg)"]
        onAccepted: {
            changeprofile(fd.fileUrl)
        }
        onRejected: {

        }
    }
    function changeprofile(str){
        user1.changeprofile(str);
    }


    FileDialog{
        id: fds
        title:"选择一个下载路径的文件夹"
        selectFolder: true
        selectMultiple: false
        selectExisting: true
        onAccepted: {
            console.log(fds.folder)
            changedownloadfolder(fds.fileUrls[0])
        }
        onRejected: {

        }
    }
    function changedownloadfolder(str){
        user1.downloadManager.path=str
//        user1.changeDownloadFolder(str);
    }


    Rectangle{
        id: registRec
        visible: false
        width: parent.width
        height: parent.height * 7.5 / 8
        Row {
            id:regist_rec_first
              spacing: 10
              anchors.topMargin:  parent.height / 10
              Text { text: qsTr("请输入用户名：") ; font.pointSize: 15
                   verticalAlignment: Text.AlignVCenter }

               Rectangle {
                   width: 150
                   height: 24
                   color: "lightgrey"
                   border.color: "grey"

                   TextInput {
                       id:registUSN
                       anchors.fill: parent
                       anchors.margins: 2
                       font.pointSize: 15
                       focus: true
                   }
               }
           }
        Row {
            id:regist_rec_second
              spacing: 10
              anchors.top: regist_rec_first.bottom
              anchors.topMargin:  parent.height / 10
              Text { text: qsTr("请输入密码：  ") ; font.pointSize: 15
                   verticalAlignment: Text.AlignVCenter }

               Rectangle {
                   width: 150
                   height: 24
                   color: "lightgrey"
                   border.color: "grey"

                   TextInput {
                       id:registPSW
                        echoMode:  TextInput.Password
                       anchors.fill: parent
                       anchors.margins: 2
                       font.pointSize: 15
                       focus: true
                   }
               }
           }
        Button{
            id:btn_regist
            anchors.top: regist_rec_second.bottom
            anchors.topMargin: parent.height / 10
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("注册")
            onClicked:  userregist(registUSN.text,registPSW.text)
        }
        Button{
            anchors.top:btn_regist.bottom
            anchors.topMargin: parent.height / 15
            anchors.horizontalCenter: parent.horizontalCenter
            id:btn_regist_quit
            text: qsTr("返回")
            onClicked:  registRec.visible = false;
        }
        onVisibleChanged: {
            registPSW.text = ""; registUSN.text = "";
        }
    }

    function userregist(str1,str2){
        if(str1 === "" || str2 === "")
        {
            user1.sendMsg("please complete your imformation");
            return;
        }

        user1.createUser(str1,str2);
        if(user1.logStatus === true){
            registRec.visible = false;
        }
    }

    Rectangle{
        id: loginRec
        visible: false
        width: parent.width
        height: parent.height * rage
        Row {
            id:login_rec_first
              spacing: 10
              anchors.topMargin:  parent.height / 10
              Text { text: qsTr("请输入用户名：") ; font.pointSize: 15
                   verticalAlignment: Text.AlignVCenter }

               Rectangle {
                   width: 150
                   height: 24
                   color: "lightgrey"
                   border.color: "grey"

                   TextInput {
                       id:loginUSN
                       anchors.fill: parent
                       anchors.margins: 2
                       font.pointSize: 15
                       focus: true
                   }
               }
           }
        Row {
            id:login_rec_second
              spacing: 10
              anchors.top: login_rec_first.bottom
              anchors.topMargin:  parent.height / 10
              Text { text: qsTr("请输入密码：  ") ; font.pointSize: 15
                   verticalAlignment: Text.AlignVCenter }

               Rectangle {
                   width: 150
                   height: 24
                   color: "lightgrey"
                   border.color: "grey"

                   TextInput {
                       id:loginPSW
                        echoMode:  TextInput.Password
                       anchors.fill: parent
                       anchors.margins: 2
                       font.pointSize: 15
                       focus: true
                   }
               }
           }
        Button{
            id:btn_login
            anchors.top: login_rec_second.bottom
            anchors.topMargin: parent.height / 10
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("登录")
            onClicked:  userlogin(loginUSN.text,loginPSW.text)
        }
        Button{
            anchors.top:btn_login.bottom
            anchors.topMargin: parent.height / 15
            anchors.horizontalCenter: parent.horizontalCenter
            id:btn_login_quit
            text: qsTr("返回")
            onClicked:  loginRec.visible = false;
        }
        onVisibleChanged: {
            loginPSW.text = "";loginUSN.text = "";
        }
    }
    function userlogin(str1,str2){
        if(str1 === "" || str2 === "")
        {
            user1.sendMsg("please complete your imformation");
            return;
        }

        user1.login(str1,str2)
        if(user1.logStatus === true){
            loginRec.visible = false;
        }
    }
    Rectangle{
        id:passwordRec
        width: parent.width
        height: parent.height* rage
        visible: false
        Row {
            id:password_rec_first
              spacing: 10
              anchors.topMargin:  parent.height / 10
              Text { text: qsTr("请输入原密码：") ; font.pointSize: 15
                   verticalAlignment: Text.AlignVCenter }

               Rectangle {
                   width: 150
                   height: 24
                   color: "lightgrey"
                   border.color: "grey"

                   TextInput {
                       id:passwordUSN
                       anchors.fill: parent
                       anchors.margins: 2
                       font.pointSize: 15
                       focus: true
                   }
               }
           }
        Row {
            id:password_rec_second
              spacing: 10
              anchors.top: password_rec_first.bottom
              anchors.topMargin:  parent.height / 10
              Text { text: qsTr("请输入新密码：") ; font.pointSize: 15
                   verticalAlignment: Text.AlignVCenter }

               Rectangle {
                   width: 150
                   height: 24
                   color: "lightgrey"
                   border.color: "grey"

                   TextInput {
                       id:passwordPSW
                       echoMode:  TextInput.Password
                       anchors.fill: parent
                       anchors.margins: 2
                       font.pointSize: 15
                       focus: true
                   }
               }
           }
        Button{
            id:btn_password
            anchors.top: password_rec_second.bottom
            anchors.topMargin: parent.height / 10
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("修改")
            onClicked:  changePSW(passwordUSN.text,passwordPSW.text);
        }
        Button{
            anchors.top:btn_password.bottom
            anchors.topMargin: parent.height / 15
            anchors.horizontalCenter: parent.horizontalCenter
            id:btn_password_quit
            text: qsTr("返回")
            onClicked:  passwordRec.visible = false;
        }
        onVisibleChanged: {
            passwordPSW.text = "";passwordUSN.text = "";
        }
    }
    function changePSW(str1,str2){
        if(str1 ==="" || str2 === ""){
            user1.sendMsg("please fill message");
            return;
        }

        if(user1.password === str1){
            user1.changepassword(str2);
            user1.sendMsg("successfully change");
            passwordRec.visible = false;
        }
        else{
            user1.sendMsg("vetify wrong");
        }
    }
}

