﻿import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import QtWebEngine 1.10
import QtQuick.Layouts 1.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.14
import QtQuick.Dialogs 1.0
import DownloadManager 1.0
import User 1.0
import "../js/MyTool.js" as MyTool
import "../component/"
Rectangle {
    id: downloadView
    color: "white"

    property var m_width: 800

    property DownloadManager manager

    Component {
        id: downloadItemDelegate
        Rectangle {
            id:rect_item
            width: listView.width
            height: 100
            border.color: "black"
            border.width: isHover == false ? 0 : 1
            radius:8

            property bool isHover: false

            color: isHover == true ? "#B0C4DE" : "white"
            state: isHover
            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                onEntered: isHover = true
                onExited: isHover = false
                onClicked: {
                    console.log("test_clicked")
                    console.log(index,listView.currentIndex)
                }

            }
            states: [
                State { name: "false"; PropertyChanges { target: rect_item; color : "white" } },
                State { name: "true"; PropertyChanges { target: rect_item; color : "#B0C4DE" }}

            ]
            transitions: [
                Transition { ColorAnimation { to: "#B0C4DE"; duration: 200 } },
                Transition { ColorAnimation { to: "white"; duration: 200  } }
            ]

            Image {
                id: image
                width: 40
                height: 40
                anchors.left: parent.left
                anchors.leftMargin: 15
                anchors.verticalCenter: parent.verticalCenter
                fillMode: Image.PreserveAspectFit
                source: MyTool.getFileIcon(modelData.name,"")
            }
            Column{
                id:column
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 15
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.right: image2.left
                anchors.rightMargin: 20
                anchors.left: image.right
                anchors.leftMargin: 20
                spacing: 15

                Text {
                    id: text1
                    width:parent.width
                    text: modelData.name
                    font.family: "Arial"
                    elide: Text.ElideRight
                    font.pixelSize: 18
                }

                Text {
                    id: text2
                    width:parent.width
                    color: "#5d5d5d"
                    elide: Text.ElideRight
                    text: modelData.path
                    font.pixelSize: 12
                }

                Text {
                    id: text3
                    width:parent.width
                    elide: Text.ElideRight
                    text: MyTool.dateToString2(modelData.time)
                    font.pixelSize: 12
                }

            }

            MyButton {
                id: image2
                width: 40
                height: 40
                m_padding: 5
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: image1.left
                anchors.rightMargin: 10
                icon_default:"../../src/folder.png"

                onClicked:{
                    //todo
                    console.log("open folder")
                    manager.open(modelData.path)
                }
            }

            MyButton {
                id: image1
                width: 40
                height: 40
                m_padding: 5
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 10
                icon_default:"../../src/delete.png"

                onClicked:{
                    // 删除下载记录（删原文件？）
                    console.log("delete history ")
                    console.log(modelData.id)
                    manager.remove(modelData.id)
                    listView.model=manager.list
                }
            }
        }

    }

    Rectangle{
        id: row_top
        height: 70

        anchors.top: parent.top
        anchors.topMargin: 0
        color:"white"
        anchors.horizontalCenter: parent.horizontalCenter
        width:m_width
        z:1
        Text{
            id:text_title
            text:"下载列表"
            font.pointSize: 18
            font.family: "Arial"
            anchors.left: parent.left
            anchors.leftMargin: 25
            anchors.verticalCenter: parent.verticalCenter
        }
        Text{
            id:download_path
            color: "#7b7b7b"
            text:manager.path
            anchors.right: btn1.left
            anchors.rightMargin: 15
            font.pointSize: 12
            font.family: "Arial"
            anchors.left: text_title.right
            anchors.leftMargin: 5
            anchors.verticalCenter: parent.verticalCenter
        }
        Button{
            id:btn1
            text:"修改文件夹"
            anchors.right: btn2.left
            anchors.rightMargin: 15
            anchors.verticalCenter: parent.verticalCenter
            onClicked: {
                console.log("folder change")
                fileDialog.visible=true
            }
        }
        Button{
            id:btn2
            text:"打开文件夹"
            anchors.right: parent.right
            anchors.rightMargin: 25
            anchors.verticalCenter: parent.verticalCenter
            onClicked: {
                console.log("open folder")
                manager.open(download_path.text)

            }
        }
    }

    ListView {
        id: listView
        anchors {
            topMargin: 30
            top: row_top.bottom
            bottom: parent.bottom
            bottomMargin: 30
            horizontalCenter: parent.horizontalCenter
        }
        width:m_width-10
        spacing: 10
        model: manager.list
        delegate: downloadItemDelegate

        Text {
            visible: !listView.count
            horizontalAlignment: Text.AlignHCenter
            height: 30
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            font.pixelSize: 20
            text: "当前暂无下载记录"
        }

        //todo:fix bug that cat not move
        ScrollBar.vertical: ScrollBar {       //滚动条
            id:scrollBar
            anchors.left: listView.right
            width: 10
            interactive:true
            active: true
            policy: ScrollBar.AsNeeded
        }
    }


    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: "file:///"+manager.path
        selectFolder: true
        selectMultiple:false
        selectExisting:true
        onAccepted: {

            manager.path=fileDialog.fileUrls[0]
        }
        onRejected: {
            console.log("Canceled")
        }
        visible : false
    }
}


